#include <iostream>
#include <stdlib.h>
using namespace std;

struct nodo{
       int nro;        // en este caso es un numero entero
       struct nodo *sgte,*ant;
};

typedef struct nodo *Tlista;

int posicionUltimoElemento(Tlista lista)
{
    int n=0;
    while(lista!=NULL){
        n++;
        lista=lista->sgte;
    }
    return n;
}

void insertarInicio(Tlista &lista, int valor)
{
    Tlista nuevo=NULL;
    nuevo=new(struct nodo);
    nuevo->nro=valor;
    nuevo->sgte=NULL;
    nuevo->ant=NULL;

    if(lista==NULL){
        lista=nuevo;
    }
    else{
        nuevo->sgte=lista;
        lista->ant=nuevo;
        lista=nuevo;
        lista->ant=NULL;
    }


}

void insertarFinal(Tlista &lista, int valor)
{
    Tlista nuevo=NULL;
    nuevo=new(struct nodo);
    nuevo->nro=valor;
    nuevo->sgte=NULL;
    nuevo->ant=NULL;

    if(lista==NULL){
        lista=nuevo;
    }
    else{
        Tlista p=lista;
        while(p->sgte!=NULL){
            p=p->sgte;
        }

        p->sgte=nuevo;
        nuevo->ant=p;
        nuevo->sgte=NULL;
    }


}

void insertarElemento(Tlista &lista, int valor, int pos)
{
    if(pos==1){
        insertarInicio(lista,valor);
    }
    else{
        if(pos==posicionUltimoElemento(lista)+1){
            insertarFinal(lista, valor);
        }
        else{
            Tlista nuevo=NULL;
            nuevo=new(struct nodo);
            nuevo->nro=valor;
            nuevo->sgte=NULL;
            nuevo->ant=NULL;

            Tlista p=lista;
            int n=1;

            while(n!=pos){
                n++;
                p=p->sgte;
            }

            nuevo->sgte=p;
            nuevo->ant=p->ant;
            p->ant=nuevo;
            nuevo->ant->sgte=nuevo;
        }

    }


}

void eliminarPrimerElemento(Tlista &lista)
{
    Tlista p=lista;
    if(lista->sgte!=NULL){
        lista=lista->sgte;
        lista->ant=NULL;
    }else{
        lista=NULL;
    }
    delete(p);
}

void eliminarUltimoElemento(Tlista &lista)
{
    Tlista p=lista;
    if(p->sgte!=NULL){
        while(p->sgte!=NULL){
            p=p->sgte;
        }
        p->ant->sgte=NULL;
    }else{
        lista=NULL;
    }
    delete(p);
}

void eliminarElemento(Tlista &lista, int pos)
{
    if(pos==1){
        eliminarPrimerElemento(lista);
    }else{
        Tlista p=lista;
        int n=1;
        while(n!=pos){
            p=p->sgte;
            n++;
        }
        if(p->sgte!=NULL){
            p->ant->sgte=p->sgte;
            p->sgte->ant=p->ant;
        }else{
            p->ant->sgte=NULL;
        }
        delete(p);
    }

}


void mostrarLista(Tlista lista)
{
     if(lista==NULL){
        cout<<"\n\tLista vacia!"<<endl;
     }
     else
     {
         int n = 0;
        Tlista p=NULL;
        while(lista!= NULL)
        {
          cout <<' '<< n+1 <<") " << lista->nro <<" ";
          p=lista;
          lista = lista->sgte;
          n++;
        }
     }

}

void menu1()
{
    cout<<"\n\t\tLISTA ENLAZADA DOBLE\n\n";
    cout<<" 1. INSERTAR AL INICIO               "<<endl;
    cout<<" 2. INSERTAR AL FINAL                "<<endl;
    cout<<" 3. INSERTAR EN UNA POSICION         "<<endl;
    cout<<" 4. ELIMINAR EL PRIMER ELEMENTO      "<<endl;
    cout<<" 5. ELIMINAR ULTIMO ELEMENTO         "<<endl;
    cout<<" 6. ELIMINAR ELEMENTO POR POSICION   "<<endl;
    cout<<" 7. QUICKSORT                        "<<endl;
    cout<<" 8. MOSTRAR LISTA                    "<<endl;
    cout<<" 0. SALIR                            "<<endl;

    cout<<"\n INGRESE OPCION: ";
}

void QuickSort(Tlista ini, Tlista fin);
void QuickSortDemo(Tlista lista){
	Tlista p = lista;
	while(p->sgte!=NULL){
        p=p->sgte;
    }
    
    QuickSort(lista,p);
}

void QuickSort(Tlista ini, Tlista fin){
	if(ini==fin){
	}
	else{
		Tlista r = fin, pivot = ini, auxN;
		int aux;
		bool derecha = true, interruptor=false;
		while(pivot!=r){
			if(derecha){
				if(r->nro < pivot->nro){
					aux=r->nro;
					r->nro=pivot->nro;
					pivot->nro=aux;
					auxN = r;
					r=pivot;
					pivot=auxN;
					derecha=false;
					r=r->sgte;
					interruptor=true;
				}else{
					r=r->ant;
				}
			}else{
				if(r->nro > pivot->nro){
					aux=r->nro;
					r->nro=pivot->nro;
					pivot->nro=aux;
					auxN = r;
					r=pivot;
					pivot=auxN;
					derecha=true;
					r=r->ant;
					interruptor=true;
				}else{
					r=r->sgte;
				} 
			}
		}
		if(interruptor){
			QuickSort(ini, pivot->ant);
		}else{
			QuickSort(pivot->sgte, fin);
		}
	}
}

/*                        Funcion Principal
---------------------------------------------------------------------*/

int main()
{
    Tlista lista = NULL;
    int op;     // opcion del menu
    int _dato;  // elemenento a ingresar
    int pos;    // posicion a insertar


    system("color 0b");

    do
    {
        menu1();  cin>> op;

        switch(op)
        {
            case 1:

                 cout<< "\n NUMERO A INSERTAR: "; cin>> _dato;
                 insertarInicio(lista, _dato);
                 mostrarLista(lista);

            break;


            case 2:

                 cout<< "\n NUMERO A INSERTAR: "; cin>> _dato;
                 insertarFinal(lista, _dato );
                 mostrarLista(lista);


            break;


            case 3:

                 cout<< "\n NUMERO A INSERTAR: ";cin>> _dato;
                 cout<< " Posicion : ";       cin>> pos;
                 while(pos<=0 || pos>posicionUltimoElemento(lista)+1){
                    cout<<"\n\t-->LA POSICION NO EXISTE!, ingrese una posicion correcta..."<<endl;
                    cout<< " Posicion : ";       cin>> pos;
                 }
                 insertarElemento(lista, _dato, pos);
                 mostrarLista(lista);

            break;


            case 4:

                if(lista==NULL){
                    cout<<"\n\t\t||Operacion invalida!||\n\n";
                }
                else{
                    eliminarPrimerElemento(lista);
                    cout<<"\n PRIMER ELEMENTO ELIMINADO..."<<endl;
                }
                mostrarLista(lista);

            break;


            case 5:

                if(lista==NULL){
                    cout<<"\n\t\t||Operacion invalida!||\n\n";
                }
                else{
                    eliminarUltimoElemento(lista);
                    cout<<"\n ULTIMO ELEMENTO ELIMINADO..."<<endl;
                }
                mostrarLista(lista);

            break;

            case 6:

                if(lista==NULL){
                    cout<<"\n\t\t||Operacion invalida!||\n\n";
                }
                else{
                    cout<< " Posicion : ";       cin>> pos;
                    while(pos<=0 || pos>posicionUltimoElemento(lista)){
                        cout<<"\n\t-->LA POSICION NO EXISTE!, ingrese una posicion correcta..."<<endl;
                        cout<< " Posicion : ";       cin>> pos;
                    }
                eliminarElemento(lista,pos);
                }
                mostrarLista(lista);

            break;

            case 7:
				
				cout<<"Lista ordenada QuickSort:"<<endl;
                QuickSortDemo(lista);
                QuickSortDemo(lista);
                mostrarLista(lista);

            break;
            
            case 8:

                cout << "\n\n MOSTRANDO LISTA\n\n";
                mostrarLista(lista);

            break;

                    }

        cout<<endl<<endl;
        system("pause");  system("cls");

    }while(op!=0);


   system("pause");
   return 0;
}
